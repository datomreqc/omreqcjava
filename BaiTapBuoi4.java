package Buoi4;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class BaiTapBuoi4 {
    //Bai 12:  Nhập tháng và năm tính ra số ngày trong tháng năm đó
    private static Scanner sc;
    public static void main(String[] strings)
    {
        Scanner input = new Scanner(System.in);
        int month;
        sc = new Scanner(System.in);

        System.out.print("Nhập số tháng từ  1 tới 12  : ");
        month = sc.nextInt();
        System.out.print("Nhập số năm: ");
        int year = input.nextInt();

        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 )
        {
            System.out.println("Tháng " + month + " Của Năm " + year + " có 31 ngày" );
        }
        else if ( month == 4 || month == 6 || month == 9 || month == 11 )
        {
            System.out.println("Tháng " + month + " Của Năm " + year + " có 30 ngày");
        }
        else if ( month == 2 )

        {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                System.out.println("Tháng " + month + " Của Năm " + year + " có 29 ngày");
            } else {
                System.out.println("Tháng " + month + " Của Năm " + year + " có 28 ngày");
            }
        }
        else
            System.out.println("Vui lòng nhập đúng số tháng từ 1 tới 12");
    }
// Bài 13: Write a Java program that keeps a number between 1 and 7 from the user and displays the name of the weekday.
    public static void cacNgayTrongTuan(String[] args) {
        double celsius;
        int choiceNumber;
        Scanner scanner = new Scanner(System.in);

        // vòng lặp for vắng mặt cả 3 biểu thức
        for (;;) {

            do {
                System.out.println("Bấm số để chọn từ 1 đến 7: ");
                choiceNumber = scanner.nextInt();
            } while ((choiceNumber < 1) || (choiceNumber > 7));

            switch (choiceNumber) {
                case 1: System.out.println("Số bạn chọn tương ứng với: Thứ Hai");break;
                case 2: System.out.println("Số bạn chọn tương ứng với: Thứ Ba");break;
                case 3: System.out.println("Số bạn chọn tương ứng với: Thứ Tư");break;
                case 4: System.out.println("Số bạn chọn tương ứng với: Thứ Năm");break;
                case 5: System.out.println("Số bạn chọn tương ứng với: Thứ Sáu");break;
                case 6: System.out.println("Số bạn chọn tương ứng với: Thứ Bẩy");break;
                case 7: System.out.println("Số bạn chọn tương ứng với: Chủ Nhật");break;
            }
        }
    }
    // Bài 14://Write a program in Java to display the pattern like right angle triangle with a number like:
    //    //1
    //    //12
    //    //123
    //    //1234
    public static void veTamgiac(String[] args)
    {
        int i,j,n;
        System.out.print("Số hàng của tam giác:n= ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();

        for(i=1;i<=n;i++)
        {
            for(j=1;j<=i;j++)
                System.out.print(j);

            System.out.println("");
        }
    }
    // Bài 15:
    // Write a program in Java to make such a pattern like right angle triangle with
    //number increased by 1.The pattern like:
    //1
    //2 3
    //4 5 6
    public static void veTamgiac2(String[] args)
    {
        int i,j,n,k=1;
        System.out.print("Số hàng của tam giác:n= ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();

        for(i=1;i<=n;i++)
        {
            for(j=1;j<=i;j++)
                System.out.print(k++);
            System.out.println("");
        }
    }
    // Bai16:
    // Write a program in Java to reverse a number.
    // Function to reverse the number

    public static void soDaoNguoc (String[] args) {
        int n, sodu, sodaonguoc = 0;
        System.out.print("Nhap vao mot so nguyen : n=");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        while(n>0){

            sodu = n%10;
            sodaonguoc = (sodaonguoc*10) + sodu;
            n = n/10;
        }

        System.out.print("Số đảo ngược của n là: "+ sodaonguoc);
    }

    //Bai17:
    // Write a Java program to display Pascal's triangle
    //    1
    //   1 1
    //  1 2 1
    // 1 3 3 1
    //1 4 6 4 1
    public static void veTamGiac3(String[] args)
    {
        int n,c=1,blk,i,j;
        System.out.print("Số hàng của tam giác:n= ");
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        for(i=0;i<n;i++)
        {
            for(blk=1;blk<=n-i;blk++)
                System.out.print(" ");
            for(j=0;j<=i;j++)
            {
                if (j==0||i==0)
                    c=1;
                else
                    c=c*(i-j+1)/j;
                System.out.print(" "+c);
            }
            System.out.print("\n");
        }
    }
    // Bài 18:
    // Write Java program to check if string with parenthesis/brackets is matching or not
    static boolean Kiemtrakitu (String expr)
    {
        Deque<Character> stack = new ArrayDeque<Character>();

        for (int i = 0; i < expr.length(); i++)
        {
            char x = expr.charAt(i);

            if (x == '(' || x == '[' || x == '{')
            {
                stack.push(x);
                continue;
            }

            if (stack.isEmpty())
                return false;
            char check;
            switch (x) {
                case ')':
                    check = stack.pop();
                    if (check == '{' || check == '[')
                        return false;
                    break;

                case '}':
                    check = stack.pop();
                    if (check == '(' || check == '[')
                        return false;
                    break;

                case ']':
                    check = stack.pop();
                    if (check == '(' || check == '{')
                        return false;
                    break;
            }
        }

        return (stack.isEmpty());
    }

    public static void kiemtra(String[] args)
    {
        System.out.print("Moi ban nhap ki tu can kiem tra ");
        Scanner in = new Scanner(System.in);
        String expr= in.nextLine();

        if (Kiemtrakitu(expr))
            System.out.println("Balanced ");
        else
            System.out.println("Not Balanced ");
    }
}




