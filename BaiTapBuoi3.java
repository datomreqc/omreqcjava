package Buoi3Array;
import java.util.Arrays;
import java.util.Scanner;

public class BaiTapBuoi3 {
    public static int[] addElement(int n, int array[], int element, int position) //position là vị trí, element là giá trị
    {
        int i;

        // Tạo bảng mới với nhiều hơn 1 phần tử
        int newarray[] = new int[n + 1];

        // chèn các phần tử từ mảng cũ vào mảng mới

        for (i = 0; i < n + 1; i++)
        {
            if (i < position - 1)
                newarray[i] = array[i];
            else if (i == position - 1)
                newarray[i] = element;
            else
                newarray[i] = array[i - 1];
        }
        return newarray;
    }

    public static void main(String[] args) {
        int n, sum = 0, tempSort;

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Nhập vào số phần tử của mảng: ");
            n = scanner.nextInt();}

        while (n < 0);
        int array[] = new int[n];

        System.out.println("Nhập các phần tử cho mảng: ");
        for (int i = 0; i < n; i++) {
            System.out.print("Nhập phần tử thứ " + i + ": ");
            array[i] = scanner.nextInt();
        }

        // Hiển thị mảng vừa nhập
        System.out.println("Mảng ban đầu: ");
        for (int i = 0; i < n; i++) {
            System.out.print(array[i] + "\t");
        }

        // Bài 7: Write a Java program to find the duplicate values of an array of string values. Tìm phần tử trùng lặp trong mảng

        System.out.println("Phần tử trùng lặp là : ");
        for (int i = 0; i < n-1; i++)
        {
            for (int j = i+1; j < n; j++)
            {
                if(( array[i] ==  array[j] )&& (i!=j)){
                System.out.println(array[j]);
            }

            }
        }

        // sắp xếp mảng theo thứ tự giảm dần
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j <= n - 1; j++) {
                if (array[i] < array[j]) {
                    tempSort = array[i];
                    array[i] = array[j];
                    array[j] = tempSort;
                }
            }
        }
        System.out.println("Mảng sắp xếp theo thứ tự giảm dần là: ");
        for (int i = 0; i < n; i++) {
            System.out.print(array[i] + "\t");
        }
        // Bài 3: Write a Java program to find the maximum and minimum value of an array."Tìm phần tử lớn nhất, nhỏ nhất của mảng"

        System.out.println("\nPhần tử nhỏ nhất trong mảng là " + array[n - 1]);
        System.out.println("\nPhần tử lớn nhất trong mảng là " + array[0]);

        // Bài 11: Write a Java program to find the second largest element in an array. "Tìm phần tử lớn thứ 2 của mảng"
        if (n>1) {
            System.out.println("Phần tử lớn thứ hai trong mảng là " + array[1]);
        } else{

            System.out.println("Không có phần tử lớn thứ hai trong mảng");
        }

        //Bài 6: Write a Java program to test if an array contains a specific value."Kiểm tra mảng có chứa giá trị cụ thể không"
        int counter;
        System.out.println("Kiểm tra mảng có chứa phần tử: item= ");
        int item = scanner.nextInt();
        for (counter = 0; counter < n; counter++)
        {
            if (array[counter] == item)
            {
                System.out.println(item+" Đang tồn tại ở vị trí "+(counter+1));
                break;
            }
        }
        if (counter == n)
            System.out.println(item + " Không tồn tại trong mảng.");

        // Bài 4: Write a Java program to insert an element (specific position) into an array."thêm phần tử element vào vị trí position"

        System.out.println("Nhập phần tử muốn thêm vào mảng :element=: ");
        int element = scanner.nextInt();

        System.out.println("Nhập vị trí muốn chèn vào mảng: position =:");
        int position = scanner.nextInt();

        array = addElement(n, array, element, position);

        // in ra mảng sau khi đã update phần tử "element" vào vị trí "position"
        System.out.println("\nArray with " + element  + " inserted at position " + position + ":\n" + Arrays.toString(array));

        // Bài 10. Write a Java program to reverse an array of integer values.
        for(int i = 0; i < array.length / 2; i++)
        {
            int temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
        System.out.println("Mảng đảo ngược là:Reverse array" + ":\n" +Arrays.toString(array));

    }

}



